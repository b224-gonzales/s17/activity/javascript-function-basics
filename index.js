// console.log("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	// first function here:
	
			function printWelcomeMessage() {
				let fullName = prompt("What is your name? ");
				let age = prompt("How old are you? ");
				let location = prompt("Where do you live? ");

				alert("Thank you for your input!");

			};

			printWelcomeMessage();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

			function showBandNames() {
				var band1 = "1. Ben and Ben";
				var band2 = "2. Apo Hiking Society";
				var band3 = "3. Silent Sanctuary";
				var band4 = "4. Eagles";
				var band5 = "5. Boys Like Girls";

				console.log(band1);
				console.log(band2);
				console.log(band3);
				console.log(band4);
				console.log(band5);
			};

			showBandNames();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

			function showMovieNamesAndRatings() {
				var movie1 = "1. Ironman (2008)";
				let mov1Rating = 'Rotten Tomatoes Rating: 94%';
				var movie2 = "2. Ironman II (2010)";
				let mov2Rating = 'Rotten Tomatoes Rating: 71%';
				var movie3 = "3. Ironman III (2013)";
				let mov3Rating = 'Rotten Tomatoes Rating: 79%';
				var movie4 = "4. John Wick";
				let mov4Rating = 'Rotten Tomatoes Rating: 86%';
				var movie5 = "5. John Wick: Chapter 2";
				let mov5Rating = "Rotten Tomatoes Rating: 89%";

				console.log(movie1);
				console.log(mov1Rating);
				console.log(movie2);
				console.log(mov2Rating);
				console.log(movie3);
				console.log(mov3Rating);
				console.log(movie4);
				console.log(mov4Rating);
				console.log(movie5);
				console.log(mov5Rating);
			};

			showMovieNamesAndRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
// let printUsers() = function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
// };


console.log(friend1);
console.log(friend2);
